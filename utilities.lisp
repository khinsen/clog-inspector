;;;; Utilities for moldable inspector
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package :clog-moldable-inspector)

(define-modify-macro callf (function &rest args)
  (lambda (value function &rest args)
    (apply function value args)))
