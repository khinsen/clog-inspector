;;;; Package definition
;;
;;;; Copyright (c) 2024-2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(defpackage :clog-moldable-inspector
  (:use :cl)
  (:import-from :arrow-macros
   :-> :-<> :->> :-<>> :<> :some-> :some->>)
  (:import-from :alexandria
   :if-let :when-let)
  (:export :clog-inspect
           :clog-inspect-package
           :clog-inspect-system
           :clog-inspect-class
           :clog-view
           :tutorial :*tutorial*))

(trivial-package-local-nicknames:add-package-local-nickname
 :hv :html-inspector-views :clog-moldable-inspector)
(trivial-package-local-nicknames:add-package-local-nickname
 :hvs :html-inspector-views/standard :clog-moldable-inspector)
