(in-package :clog-moldable-inspector)

;; CL-USER> (ql:quickload :clog-moldable-inspector/tools)
;; To load "clog-moldable-inspector/tools":
;;     clog-moldable-inspector/tools
;; ; Loading "clog-moldable-inspector/tools"
;; ..................................................
;; [package clog-moldable-inspector]....
;; (:CLOG-MOLDABLE-INSPECTOR/TOOLS)
;; CL-USER> (clog-tools:clog-builder)

(clog-tools:add-inspector "Moldable Inspector"
                          (lambda (symbol title value clog-obj)
                            (clog-inspect :object symbol :clog-obj clog-obj)))

(clog-tools:add-file-extension "Moldable Inspector"
                               (lambda (file dir project clog-obj)
                                 (when file
                                   (clog-inspect :object file :clog-obj clog-obj))
                                 (when dir
                                   (clog-inspect :object dir :clog-obj clog-obj))))
