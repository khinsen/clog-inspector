;;;; System definition
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(asdf:defsystem #:clog-moldable-inspector
  :description "A moldable object inspector based on CLOG"
  :author "Konrad Hinsen <konrad.hinsen@fastmail.net>"
  :license  "GPL"
  :version "0.0.1"
  :serial t
  :depends-on (#:clog #:clog-ace
               #:3bmd #:3bmd-ext-code-blocks
               #:fset
               #:html-inspector-views
               #:html-inspector-views/standard
               #:alexandria
               #:arrow-macros
               #:str
               #:trivial-package-local-nicknames)
  :components ((:file "package")
               (:file "utilities")
               (:file "svg-buttons")
               (:file "inspector")
               (:file "views")
               (:file "playground")
               (:file "tutorial")))

(asdf:defsystem #:clog-moldable-inspector/tools
  :depends-on (#:clog-moldable-inspector
               #:clog/tools)
  :components ((:file "builder-install")))
