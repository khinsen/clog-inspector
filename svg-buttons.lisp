;;;; SVG buttons used in inspector panes
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package :clog-moldable-inspector)

(defvar *svg-directory* 
  (asdf:system-relative-pathname :clog-moldable-inspector "svg/"))

(defun load-svg-icon (filename)
  (-> filename
      (merge-pathnames *svg-directory*)
      alexandria:read-file-into-string))

(defvar *icon-close* (load-svg-icon "icon-close.svg"))
(defvar *icon-maximize* (load-svg-icon "icon-maximize.svg"))
(defvar *icon-minimize* (load-svg-icon "icon-minimize.svg"))
(defvar *icon-refresh* (load-svg-icon "icon-refresh.svg"))

