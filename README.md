# clog-moldable-inspector

A [moldable](https://moldabledevelopment.com/) Common Lisp object inspector based on [CLOG](https://github.com/rabbibotton/clog). The inspector is thus shown in a Web browser.

*Warning:* this package is in active development. Many details are likely to change in the future. I am not making any promises about backwards compatibility at this time.

## Dependencies

You can install `clog-moldable-inspector` from [Ultralisp](http://ultralisp.org/). If you prefer a more manual installation, here is the list of dependencies:

 - [clog](https://github.com/rabbibotton/clog/), which you can install via Quicklisp or by any other means you may prefer.
  - [clog-ace](https://github.com/rabbibotton/clog/), which you should get directly from GitHub because the inspector depends on recent additions to it.
 - [html-inspector-views](https://codeberg.org/khinsen/html-inspector-views), which defines the (small) infrastructure for defining views and also most of the views used by the inspector. Separate systems and packages for view definitions avoid making clog-moldable-inspector and thus clog dependencies of every piece of code that implements custom views.

## Usage

```
(asdf:load-system :clog-moldable-inspector)
(clog-moldable-inspector:tutorial)
```

<img src="screenshot-tutorial.png" height="600" />

## Examples of view definitions

[html-inspector-views](https://codeberg.org/khinsen/html-inspector-views) contains many views for data types from the Common Lisp standard (lists, vectors, hash tables, pathnames, systems, packages, etc.). The names of view definitions have a common prefix (the ["eyes" emoji](https://emojipedia.org/eyes) 👀), so they are easy to spot.

My [malleable-message-manager](https://codeberg.org/khinsen/malleable-message-manager) has views for various Mastodon data types in the file [inspector-views.lisp](https://codeberg.org/khinsen/malleable-message-manager/src/branch/main/inspector-views.lisp).

## Motivation

While this is a generic Common Lisp object inspector, much like [Clouseau](https://mcclim.common-lisp.dev/static/manual/mcclim.html#Inspector)), my motivation for developing it is more specific: I want to make it easier to present [computer-aided research](https://science-in-the-digital-era.khinsen.net/#Computer-aided%20research) with a large [cognitive surface](https://science-in-the-digital-era.khinsen.net/#The%20cognitive%20surface%20of%20software). In more familiar terms, I want to empower scientists to share their code, data, and associated narratives in a way that makes them comprehensible to their peers.

For a simple example, see [ratp-points-of-sale](https://codeberg.org/khinsen/ratp-points-of-sale).

## License

BSD

## Acknowledgment

This inspector is heavily influenced by the moldable inspector in [Glamorous Toolkit](https://gtoolkit.com/).
